import * as React from "react";
import { btoa } from "abab";
import ServerDataTag from "./ServerDataTag";

const ServerDataContext: React.Context<ServerDataStore> = React.createContext();
export { ServerDataContext };

export type ActionFunction = (...args: any[]) => Promise<any>;

export interface IAction {
    [index: string]: ActionFunction;
}

export class ServerDataStore {
    private static actions: IAction = {};
    private dataLoaders: { [index: string]: any } = {};
    private tagRenderer: ServerDataTag;
    public static isServer = !global.window;
    private isFirstRun: boolean = true;

    /** Queues an action to be run either immedietly (client) or after render (server) */
    public runAction(name: string, ...args: any[]) {
        if (!ServerDataStore.isServer) {
            return;
        }
        const b64 = name + (btoa(JSON.stringify(args)) || "");
        if (!this.dataLoaders[b64]) {
            this.dataLoaders[b64] = {
                name,
                args,
            };
        }
    }

    /** Server side version, for client version use the static method
     *  Runs a queued action and returns the result
     */
    public async getResult(name: string, ...args: any[]) {
        return ServerDataStore.actions[name](...args);
    }

    /** Client side version, for server version use the context method
    *   Runs a queued action and returns the result, or fetches from head data if already present
    */
    public static async getResult(name: string, ...args: any[]) {
        const b64 = name + (btoa(JSON.stringify(args)) || "");
        if (global.__pushData && global.__pushData[b64] !== undefined) {
            return global.__pushData[b64];
        }
        return ServerDataStore.actions[name](...args);
    }

    public async getDataAsString() {
        const promises = Object.keys(this.dataLoaders).map((key) => {
            return ServerDataStore.actions[this.dataLoaders[key].name](...this.dataLoaders[key].args);
        });
        const data = await Promise.all(promises);
        return `<script>
    window.__pushData = {}
        ${Object.keys(this.dataLoaders).map((key, index) => {
            return `window.__pushData['${key}'] = ${JSON.stringify(data[index]).replace(/</g, `\\u003c`)}`;
        })}
</script>`;
    }

    public async getTagsAsString() {
        if (this.tagRenderer) {
            return this.tagRenderer.renderTags();
        }
        return "";
    }

    public static registerAction(name: string, resolver: ActionFunction) {
        ServerDataStore.actions[name] = resolver;
    }

    public registerTagRenderer(renderer: ServerDataTag) {
        if (ServerDataStore.isServer) {
            this.tagRenderer = renderer;
        }
    }

    public get firstRun() {
        const val = this.isFirstRun;
        this.isFirstRun = false;
        return val
    }
}