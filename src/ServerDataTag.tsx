import * as React from "react";
import { ServerDataContext, ServerDataStore } from "./ServerDataContext";
import { renderToString } from "react-dom/server";

export interface IServerDataTagsProps {
    action?: string;
    args?: any;
}

export default class ServerDataTag extends React.PureComponent<IServerDataTagsProps> {
    static contextType = ServerDataContext;
    context: React.ContextType<typeof ServerDataContext>;

    public async renderTags() {
        const { children } = this.props;
        if (!children) {
            return;
        }
        if (typeof children !== "function") {
            throw new Error("PushDataTags must be provided a function");
        }

        const action = this.props.action;
        if (!action) {
            return renderToString(children());
        }
        const args = this.props.args || [];
        return renderToString(children(await this.context.getResult(action, ...args))).replace(/<!-- -->/g, "");
    }

    public async componentDidMount() {
        if (this.context.firstRun || !document.head) {
            return;
        }
        const { children, action } = this.props;
        if (!children) {
            return;
        }
        if (typeof children !== "function") {
            throw new Error("PushDataTags must be provided a function");
        }
        let newHead = "";
        if (!action) {
            newHead = renderToString(children());
        } else {
            const args = this.props.args || [];
            newHead = renderToString(children(await ServerDataStore.getResult(action, ...args))).replace(/<!-- -->/g, "");
        }
        const oldItems = document.head.querySelectorAll("[data-reactroot]");
        oldItems.forEach((item) => {
            if (item) {
                const parent = item.parentNode;
                if (parent) {
                    parent.removeChild(item);
                }
            }
        });
        document.head.innerHTML += newHead;
    }

    public render() {
        if (this.props.action) {
            const args = this.props.args || [];
            this.context.runAction(this.props.action, ...args);
        }
        this.context.registerTagRenderer(this);
        return null;
    }
}